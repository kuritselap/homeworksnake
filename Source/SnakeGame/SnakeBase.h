// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;
class ASpecialFruit;
class ABordersBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT

};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	//UPROPERTY(EditDefaultsOnly)
		//float MovementSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	//next two variables is to spawn food
	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	//next two variables is to spawn specialfruit
	UPROPERTY(BlueprintReadWrite)
		ASpecialFruit* SpecialfruitActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpecialFruit> SpecialfruitActorClass;

	//coordinats for spawning fruit location
	float MinX = -200.f;
	float MaxX = 200.f;
	float MinY = -200.f;
	float MaxY = 200.f;
	float SpawnZ = 0.f;

	//var for check food spawn
	int CurrentSpawnedFruit;

	//counter for eaten fruits to spawn special fruit
	int32 EatenFruits = 0;

	//Deley between steps
	float Delay;

	//Buffer for tick
	float TickBuffer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	//spawns food for snake
	void CreateFood(int32 AmountOfEatenFruits);

	//Sets current value of spawned fruits
	void SetFood(int CurrentValue = 0);

	//returns changed value of fruits
	int GetFood();

	//next two functions to count eaten fruits
	//set
	void EatenFruitsIncreaser();

	//get
	int32 EatenFruitsSender();

	//method for changing movement speed
	void DelayChanger();

};
