// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "SpecialFruit.h"
#include "BordersBase.h"
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	Delay = 1.0f;
	TickBuffer = 0;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//set movement speed
	TickBuffer += DeltaTime;
	if (TickBuffer > Delay)
	{
		Move();
		TickBuffer = 0;

	}

	CreateFood(EatenFruitsSender());

}


void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}

	}

}


void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;

	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector); // SetActorLocation(GetActorLocation() + MovementVector)
	SnakeElements[0]->ToggleCollision();

}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);

		}

	}

}

//this thing spawns food
void ASnakeBase::CreateFood(int32 AmountOfEatenFruits)
{
	int AllowedAmoutOfFruit = 1;
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxY);
	FVector SpawnSpot(SpawnX, SpawnY, SpawnZ);
	FTransform SSTransform(SpawnSpot);

	if (GetFood() != AllowedAmoutOfFruit && AmountOfEatenFruits == 5)
	{
		SpecialfruitActor = GetWorld()->SpawnActor<ASpecialFruit>(SpecialfruitActorClass, SSTransform);
		SetFood(1);

	}

	else if (GetFood() != AllowedAmoutOfFruit && AmountOfEatenFruits != 5)
	{
		FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, SSTransform);
		SetFood(1);

	}

}

//Sets current value of spawned fruits
void ASnakeBase::SetFood(int CurrentValue)
{
	CurrentSpawnedFruit = CurrentValue;

}

//returns changed value of fruits
int ASnakeBase::GetFood()
{
	return int(CurrentSpawnedFruit);

}

//next two functions to count eaten fruits
//set
void ASnakeBase::EatenFruitsIncreaser()
{
	EatenFruits += 1;

}

//get
int32 ASnakeBase::EatenFruitsSender()
{
	return int32(EatenFruits);

}

//method for changing movement speed
void ASnakeBase::DelayChanger()
{
	Delay -= 0.1;

}
